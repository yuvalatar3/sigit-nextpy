class MusicNotes:
    def __init__(self):
        self._notes_freqs = [55, 61.74, 65.41, 73.42, 82.41, 87.31, 98]
        self._notes_idx = -1
        self._counter = 0

    def __iter__(self):
        return self

    def __next__(self):
        if self._counter * (self._notes_idx + 1) == len(self._notes_freqs) * 4:
            raise StopIteration
        if self._notes_idx < len(self._notes_freqs) - 1:
            self._notes_idx += 1
        else:
            self._notes_idx = 0
            self._counter += 1
        self._notes_freqs[self._notes_idx] *= 2
        return self._notes_freqs[self._notes_idx] / 2


def main():
    notes_iter = iter(MusicNotes())
    for freq in notes_iter:
        print(freq)


if __name__ == '__main__':
    main()

