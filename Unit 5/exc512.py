import winsound


def play_song():
    freqs = {"la": 220,
             "si": 247,
             "do": 261,
             "re": 293,
             "mi": 329,
             "fa": 349,
             "sol": 392,
             }
    notes = "sol,250-mi,250-mi,500-fa,250-re,250-re,500-do,250-re,250-mi,250-fa,250-sol,250-sol,250-sol,500"
    all_notes = notes.split("-")
    print(type(all_notes))  # To make sure its iterable
    print(dir(all_notes))  # To make sure its iterable
    for note in all_notes:
        note_details = note.split(",")
        winsound.Beep(freqs[note_details[0]], int(note_details[1]))


def main():
    play_song()


if __name__ == '__main__':
    main()
