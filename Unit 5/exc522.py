def every_third_number():
    numbers = iter(list(range(1, 101)))

    for i in numbers:
        print(i)
        try:
            next(numbers)
            next(numbers)
        except StopIteration as ex1:
            break


def main():
    every_third_number()


if __name__ == '__main__':
    main()
