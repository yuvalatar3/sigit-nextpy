def translate(sentence):
    words = {'esta': 'is', 'la': 'the', 'en': 'in', 'gato': 'cat', 'casa': 'house', 'el': 'the'}
    translation_generator = (words[word] for word in sentence.split(" "))
    translated_sentence = ""
    for word in translation_generator:
        translated_sentence += word + " "
    return translated_sentence


def main():
    print(translate("el gato esta en la casa"))


if __name__ == '__main__':
    main()
