def get_fibo():
    print("Stating fibonacci now!")
    n1 = 0
    n2 = 1
    yield n1
    yield n2
    while True:
        yield n1 + n2
        n1, n2 = n2, n1 + n2


def main():
    fibo_gen = get_fibo()
    print(next(fibo_gen))
    print(next(fibo_gen))
    print(next(fibo_gen))
    print(next(fibo_gen))


if __name__ == '__main__':
    main()
