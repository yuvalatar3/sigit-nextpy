from file1 import GreetingCard
from file2 import BirthdayCard


def main():
    yay = GreetingCard("Yuval", "Atar")
    hurray = BirthdayCard("Yuval", "Haim", 18)
    yay.greeting_msg()
    hurray.greeting_msg()


if __name__ == '__main__':
    main()
