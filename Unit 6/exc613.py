import tkinter as tk


window = tk.Tk()


def button_click():
    img_path = r"..\soccer.png"
    img = tk.PhotoImage(file=img_path)
    image_label = tk.Label(window, image=img)
    image_label.image = img
    image_label.pack()


def main():
    question = tk.Label(window, text="What is yuval's favorite game?")
    question.pack()

    button = tk.Button(window, text="Reveal answer!", command=button_click)
    button.pack()

    window.mainloop()


if __name__ == '__main__':
    main()
