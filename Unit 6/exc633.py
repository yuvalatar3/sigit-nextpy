import pyttsx3


def main():
    engine = pyttsx3.init()

    # Set the properties
    engine.setProperty('rate', 150)  # Speed of speech
    engine.setProperty('volume', 0.8)  # Volume level (0.0 to 1.0)
    voices = engine.getProperty('voices')

    # Convert text to speech
    text = "first time i'm using a package in next.py course"
    engine.say(text)

    engine.runAndWait()


if __name__ == '__main__':
    main()
