def dup(char):
    return char * 2


def double_letter(my_str):
    print(''.join(list(map(dup, my_str))))


if __name__ == '__main__':
    double_letter("We are the champions!")
