def is_prime(number):
    return len([item for item in range(2, number) if number % item == 0]) == 0


if __name__ == '__main__':
    print(is_prime(42))
    print(is_prime(43))
