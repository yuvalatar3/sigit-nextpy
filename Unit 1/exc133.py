def is_funny(string):
    return len([item for item in string if item != 'a' if item != 'h']) == 0


if __name__ == '__main__':
    print(is_funny("hahahahahaha"))
