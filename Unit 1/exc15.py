import functools


def longest_name(all_names):
    return sorted(all_names.split("\n"), key=lambda name: len(name))[len(all_names.split("\n")) - 1]


def summarized_lengths(all_names):
    return functools.reduce(lambda n1, n2: n1 + n2, map(lambda name: len(name), all_names.split("\n")))


def all_shortest(all_names):
    sorted_names = sorted(all_names.split("\n"), key=lambda name: len(name))
    min_len = len(sorted_names[0])
    return '\n'.join([name for name in sorted_names if len(name) == min_len])


def save_lengths(all_names):
    with open("name_length.txt", 'w') as lengths_file:
        lengths_file.write("\n".join(list(map(lambda name: str(len(name)), all_names.split("\n")))))


def get_lengths(all_names):
    length = input("Enter name length: ")
    return "\n".join(list(filter(lambda name: len(name) == int(length), all_names.split("\n"))))


if __name__ == '__main__':
    with open("../names.txt", 'r') as file:
        file_content = file.read()
        print(longest_name(file_content))
        print(summarized_lengths(file_content))
        print(all_shortest(file_content))
        save_lengths(file_content)
        print(get_lengths(file_content))
