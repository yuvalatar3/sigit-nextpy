def solve(pas):
    print(''.join([let if let == ' ' or let == ':' else chr(ord(let) + 2) for let in pas]))


if __name__ == '__main__':
    password = "sljmai ugrf rfc ambc: lglc dmsp mlc rum"
    solve(password)
    