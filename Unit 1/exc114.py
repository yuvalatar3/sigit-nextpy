import functools


def add_digit(n1, n2):
    return int(n1) + int(n2)


def sum_of_digits(number):
    return functools.reduce(add_digit, str(number))


if __name__ == '__main__':
    print(sum_of_digits(104))
