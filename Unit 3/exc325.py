def read_file(file_name):
    mah = "__CONTENT_START__\n"
    try:
        file = open(file_name, 'r')
    except IOError:
        mah += "__NO_SUCH_FILE__\n"
    else:
        mah += file.read()
    finally:
        mah += "__CONTENT_END__\n"
        return mah


def main():
    print(read_file("test.txt"))
    print(read_file("tsest.txt"))


if __name__ == '__main__':
    main()
