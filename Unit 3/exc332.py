class UnderAge(Exception):
    def __init__(self, arg):
        self._arg = arg

    def __str__(self):
        return f"Sorry but {self._arg} is not enough, you should be 18 or more.\n" \
               f"You will be able to join to the party in {18 - self._arg} years."

    def get_arg(self):
        return self._arg


def send_invitation(name, age):
    try:
        if int(age) < 18:
            raise UnderAge(int(age))
    except UnderAge as ex1:
        print(ex1)
    else:
        print("You shuold send an invite to " + name)


def main():
    send_invitation("Yuval", 17)
    send_invitation("Atar", 20)


if __name__ == '__main__':
    main()
