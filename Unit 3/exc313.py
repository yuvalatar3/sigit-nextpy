def e1():
    y = [1, 2, 3]
    x = iter(y)
    print(x.__next__())
    print(x.__next__())
    print(x.__next__())
    print(x.__next__())


def e2():
    number = 5 / 0


def e3():
    n1 = 5
    n2 = 0
    assert n2 != 0
    return n1 / n2


def e4():
    # import yuvalPack - To prevent compilation errors:
    pass


def e5():
    my_dict = {}
    my_name = my_dict["Yuval"]


def e6():
    # while True - To prevent compilation errors:
    pass


def e7():
    print("Lets")
    #    print("Go") - To prevent compilation errors.


def e8():
    my_num = "Yuval" / 5


def main():
    e8()


if __name__ == '__main__':
    main()
