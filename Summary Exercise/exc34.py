import string


class UsernameContainsIllegalCharacter (Exception):
    def __init__(self, character, index):
        self._character = character
        self._index = index

    def __str__(self):
        return f"The username contains an illegal character, which is '{self._character}' at index {self._index}."


class UsernameTooShort (Exception):
    def __init__(self):
        super().__init__()

    def __str__(self):
        return "Username has be at least 3 characters."


class UsernameTooLong (Exception):
    def __init__(self):
        super().__init__()

    def __str__(self):
        return "Username couldn't be more than 16 characters."


class PasswordMissingCharacter (Exception):
    def __init__(self):
        super().__init__()

    def __str__(self):
        return f"The password is missing a character"


class CapitalPasswordMissingCharacter (PasswordMissingCharacter):
    def __init__(self):
        super().__init__()

    def __str__(self):
        return super().__str__() + " (Uppercase)"


class LowerPasswordMissingCharacter (PasswordMissingCharacter):
    def __init__(self):
        super().__init__()

    def __str__(self):
        return super().__str__() + " (Lowercase)"


class NumberPasswordMissingCharacter (PasswordMissingCharacter):
    def __init__(self):
        super().__init__()

    def __str__(self):
        return super().__str__() + " (Digit)"


class SignPasswordMissingCharacter (PasswordMissingCharacter):
    def __init__(self):
        super().__init__()

    def __str__(self):
        return super().__str__() + " (Special)"


class PasswordTooShort (Exception):
    def __init__(self):
        super().__init__()

    def __str__(self):
        return "Password has be at least 8 characters."


class PasswordTooLong (Exception):
    def __init__(self):
        super().__init__()

    def __str__(self):
        return "Password couldn't be more than 40 characters."


def check_input(username, password):
    try:
        counter = 0
        for letter in username:
            if not letter.isalpha() and not letter.isdigit() and letter != '_':
                raise UsernameContainsIllegalCharacter(letter, counter)
            counter += 1
    except UsernameContainsIllegalCharacter as ex1:
        print(ex1)
    else:
        try:
            if len(username) < 3:
                raise UsernameTooShort
        except UsernameTooShort as ex2:
            print(ex2)
        else:
            try:
                if len(username) > 16:
                    raise UsernameTooLong
            except UsernameTooLong as ex3:
                print(ex3)
            else:
                try:
                    if len(password) < 8:
                        raise PasswordTooShort
                except PasswordTooShort as ex4:
                    print(ex4)
                else:
                    try:
                        if len(password) > 40:
                            raise PasswordTooLong
                    except PasswordTooLong as ex5:
                        print(ex5)
                    else:
                        try:
                            if not any(char.isupper() for char in password):
                                raise CapitalPasswordMissingCharacter
                        except CapitalPasswordMissingCharacter as ex6:
                            print(ex6)
                        else:
                            try:
                                if not any(char.islower() for char in password):
                                    raise LowerPasswordMissingCharacter
                            except LowerPasswordMissingCharacter as ex7:
                                print(ex7)
                            else:
                                try:
                                    if not any(char.isdigit() for char in password):
                                        raise NumberPasswordMissingCharacter
                                except NumberPasswordMissingCharacter as ex8:
                                    print(ex8)
                                else:
                                    try:
                                        if not any(char in string.punctuation for char in password):
                                            raise SignPasswordMissingCharacter
                                    except SignPasswordMissingCharacter as ex9:
                                        print(ex9)
                                    else:
                                        print("OK")


def main():
    check_input("1", "2")
    check_input("0123456789ABCDEFG", "2")
    check_input("A_a1.", "12345678")
    check_input("A_1", "2")
    check_input("A_1", "ThisIsAQuiteLongPasswordAndHonestlyUnnecessary")
    check_input("A_1", "abcdefghijklmnop")
    check_input("A_1", "ABCDEFGHIJLKMNOP")
    check_input("A_1", "ABCDEFGhijklmnop")
    check_input("A_1", "4BCD3F6h1jk1mn0p")
    check_input("A_1", "4BCD3F6.1jk1mn0p")


if __name__ == '__main__':
    main()
