def check_id_valid(id_number):
    """
    Checks if a certain sequence of number is valid as ID number.
    :param id_number: The current ID that needs to be checked.
    :type id_number: int
    :return: True if the ID is valid, otherwise returns False.
    :rtype: bool
    """
    new_id = ""
    current = 1
    digits_sum = 0
    for num in str(id_number):
        if current % 2 == 0:
            digit = int(num) * 2
            if digit > 9:
                digit = digit % 10 + digit // 10
            new_id += str(digit)
        else:
            new_id += num
        current += 1
    for dig in new_id:
        digits_sum += int(dig)
    return digits_sum % 10 == 0


class IDIterator:
    """
    A class used to iterate on the available and valid IDs.
    """

    def __init__(self, typed_id):
        """
        Basic constructor which initialization to _id, according the passed parameter.
        :param typed_id: The ID the user entered before in the main function.
        :type typed_id: int
        """
        self._id = typed_id

    def __iter__(self):
        """
        Define an iterable behavior to IDIterator's class.
        :return:None
        """
        return self

    def __next__(self):
        """
        Handles the iterations when we want to iterate to the next available ID.
        :return: None
        :raise: StopIteration: In case we reached the ID limit (which is 999999999).
        """
        for current_id in range(self._id + 1, 999999999):
            if check_id_valid(current_id):
                self._id = current_id
                return current_id
        raise StopIteration


def id_generator(typed_id):
    """
    Generator function which yields all the available and valid IDs after a certain ID the user has typed before.
    :param typed_id: The ID the user entered before in the main function.
    :type typed_id: int
    :return: The current valid ID, the generator yielded.
    :rtype: int
    """
    for crr_id in range(typed_id + 1, 999999999):
        if check_id_valid(crr_id):
            yield crr_id


def main():
    best_id = int(input("Enter ID:"))
    decision = input("Generator or Iterator? (gen/it)? ")
    if decision == "it":
        current = iter(IDIterator(best_id))
    if decision == "gen":
        current = id_generator(best_id)
    counter = 0
    while counter < 10:
        print(next(current))
        counter += 1


if __name__ == '__main__':
    main()
