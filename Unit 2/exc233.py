class Lion:
    count_animals = 0

    def __init__(self, age, name="Octavio"):
        self._name = name
        self._age = age
        Lion.count_animals += 1

    def birthday(self):
        self.age += 1

    def get_age(self):
        return self.age

    def set_name(self, name):
        self._name = name

    def get_name(self):
        return self._name


def main():
    first_lion = Lion(18, "Superbo")
    second_lion = Lion(20)
    print(first_lion.get_name())
    print(second_lion.get_name())
    second_lion.set_name("Super")
    print(second_lion.get_name())
    print(Lion.count_animals)


if __name__ == '__main__':
    main()
