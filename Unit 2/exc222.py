class Lion:
    def __init__(self):
        self.name = "Octavio"
        self.age = 18

    def birthday(self):
        self.age += 1

    def get_age(self):
        return self.age


def main():
    first_lion = Lion()
    second_lion = Lion()
    first_lion.birthday()
    print(first_lion.get_age())
    print(second_lion.get_age())


if __name__ == '__main__':
    main()
